package src.main.java.pl.sdacademy;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-09-29.
 */
//tworzymy generator htmla, ten prjekt bedzi epodlaczony pod ten z gra, sam nie dziala,
// bedzie wytwarzac szablon strony,
// a my to co w body w innym projekcie bedziemy wstawiac
public class HtmlGenerator {

    public static void generateHeader(HttpServletResponse response, String pageTitle) throws IOException {
        generateHeader(response, pageTitle, new ArrayList<String>());
    }
    public static void generateHeader(HttpServletResponse response, String pageTitle, List<String> metaTags) throws IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        generateHeader(response.getWriter(), pageTitle, metaTags);
    }

    public static void generateHeader(PrintWriter printWriter, String pageTitle){
        generateHeader(printWriter, pageTitle, new ArrayList<String>());
    }

    public static void generateHeader(PrintWriter printWriter, String pageTitle, List<String> metaTags){
    printWriter.println("<!DOCTYPE html>");
    printWriter.println("<html lang=pl>");
    printWriter.println("<head>");
    printWriter.println("<title>" + pageTitle + "</title>");

    for (String tag : metaTags){
        printWriter.println("<meta " + tag + ">");
    }
    printWriter.println("</head>");
    printWriter.println("<body>");
    }

    public static void generateFooter(PrintWriter printWriter){
    printWriter.println("</body>");
    printWriter.println("</html>");
    }
}
